var gulp = require('gulp');
var minifyCSS = require('gulp-minify-css');
var minifyJs = require('gulp-jsmin');
var concat = require('gulp-concat');

gulp.task('minifyCSS', function() {
    gulp.src([
        'assets/css/font-opensans.css',
        'assets/css/font-montserrat.css',
        'assets/css/font-sourcepro.css',
        'assets/css/font-awesome.css',
        'assets/css/base.css',
        'assets/css/grid.css',
        'assets/css/owl.carousel.css',
        'assets/css/jquery.fancybox.css',
        'assets/css/style.css'
    ])
    .pipe(minifyCSS())
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('assets/css'));
});

gulp.task('minifyJs', function() {
    gulp.src([
        'assets/js/jquery-latest.min.js',
        'assets/js/jquery.fancybox.js',
        'assets/js/owl.carousel.js',
        'assets/js/scripts.js'
    ])
    .pipe(minifyJs())
    .pipe(concat('scripts.min.js'))
    .pipe(gulp.dest('assets/js'));    
});

gulp.task('default', ['minifyCSS', 'minifyJs']);