/* CSS Document
*
* @autor Fernanda Pieretti
* @data ago2016
* @ref scripts.js
*
*/

$(window).load(function(){	
	//Menu
	$("#menuDropDown").click(function() {
		if ($("#menuMobile").hasClass("ativo")) {
			$("#menuMobile").removeClass("ativo");
			$("#bustaTopo").css("display","none");			
		} else {
			$("#menuMobile").addClass("ativo");
			$("#bustaTopo").css("display","block");
		}
	});
	
	//Galeria
	$(".fancybox").fancybox();
	$(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	
	//Sliders
	$("#parceiros-slider").owlCarousel({ 
      navigation : true,
	  pagination: false,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem: true,
      accessibility: false
  });
  $("#noticia-slider").owlCarousel({ 
      navigation : true,
	  pagination: false,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem: true,
      accessibility: false
  });
});
